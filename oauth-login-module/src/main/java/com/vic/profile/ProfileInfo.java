package com.vic.profile;

/**
 * Created with IntelliJ IDEA.
 * User: Vic
 * Date: 6/09/13
 * Time: 14:15
 * To change this template use File | Settings | File Templates.
 */
public interface ProfileInfo {
    public String getId();
    public String getEmail();
    public String getGiven_name();
    public String getFamily_name();
}
