package com.vic.profile;

/**
 * Created with IntelliJ IDEA.
 * User: Vic
 * Date: 5/09/13
 * Time: 13:47
 * To change this template use File | Settings | File Templates.
 */
public class LinkedInProfileInfo implements ProfileInfo {
    private String email;
    private String name;
    private String given_name;
    private String family_name;

    public String getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public String getName() {
        return name;
    }

    public String getGiven_name() {
        return given_name;
    }

    public String getFamily_name() {
        return family_name;
    }



    private String id;

    public void setId(String id) {
        this.id = id;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setGiven_name(String given_name) {
        this.given_name = given_name;
    }

    public void setFamily_name(String family_name) {
        this.family_name = family_name;
    }




}
