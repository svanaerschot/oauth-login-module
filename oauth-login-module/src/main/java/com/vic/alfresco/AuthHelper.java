package com.vic.alfresco;

import org.scribe.model.Token;

import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: Vic
 * Date: 4/09/13
 * Time: 15:46
 * To change this template use File | Settings | File Templates.
 */
public interface AuthHelper {
    public String buildLoginUrl(String callbackUri, Token requestToken);
    public String getUserInfo(final String authCode, String callbackUri, Token requestToken)throws IOException;

}
