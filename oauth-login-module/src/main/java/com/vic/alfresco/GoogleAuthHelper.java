package com.vic.alfresco;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeRequestUrl;
import com.google.api.client.googleapis.auth.oauth2.GoogleTokenResponse;
import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestFactory;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson.JacksonFactory;

import java.io.IOException;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.List;

/*
 * Author: Sven Van Aerschot
 *
 * Sources:
 * https://github.com/mdanter/OAuth2v1 by mdanter
 * https://developers.google.com/accounts/docs/OAuth2Login
 *
 *
 * Remove comments around code for secure state functionality
 * (switch statements in buildLoginUrl)
 *
 * NOTE: This class uses Google Libraries for Google OAuth
 * For general OAuth usage (with Google, LinkedIn & Twitter,
 * please refer to ScribeAuthHelper
 */

public class GoogleAuthHelper{
    /*
     * Google API information is retrieved by registering your app at
     * https://code.google.com/apis/console/
     * These are entered in the share-config-custom.xml typically found in
     * tomcat/shared/classes/alfresco/web-extension
     */

    /*
    private static final String CLIENT_ID= "";
    private static final String CLIENT_SECRET = "";
    private static final String CALLBACK_URI = "";
      */
    //API calls the application is allowed to make
    private static final List<String> SCOPE = Arrays.asList("https://www.googleapis.com/auth/userinfo.profile;https://www.googleapis.com/auth/userinfo.email".split(";"));

    private static final String USER_INFO_URL = "https://www.googleapis.com/oauth2/v1/userinfo";
    private static final JsonFactory JSON_FACTORY = new JacksonFactory();
    private static final HttpTransport HTTP_TRANSPORT = new NetHttpTransport();

        /*
         *  Optional but strongly recommended
         */
    //private String stateToken;

    private final GoogleAuthorizationCodeFlow flow; //Thread-safe Google OAuth 2.0 authorization code flow that manages and persists end-user credentials.

    /*
    OAuthSSOAuthenticationFilter reads clientID and clientSecret from the
    XML file specified above.
     */
    public GoogleAuthHelper(String clientID, String clientSecret){
        flow = new GoogleAuthorizationCodeFlow.Builder(HTTP_TRANSPORT,
                JSON_FACTORY, clientID, clientSecret, SCOPE).build();
        //      generateStateToken();
    }

    public String getUserInfo(String test, String testd){
        return "d";
    }

     /*
     This is a deprecated constructor in case clientID and clientSecret are
     hardcoded in this class
     */

    /*
    public GoogleAuthHelper(){
        flow = new GoogleAuthorizationCodeFlow.Builder(HTTP_TRANSPORT,
                JSON_FACTORY, CLIENT_ID, CLIENT_SECRET, SCOPE).build();
        //      generateStateToken();
    }  */



    public String buildLoginUrl(String callbackUri){
        final GoogleAuthorizationCodeRequestUrl url = flow.newAuthorizationUrl();
        return url.setRedirectUri(callbackUri).build();
        //      return url.setRedirectUri(CALLBACK_URI).setState(stateToken).build();
    }


    //CallbackUri is read from share-config-custom
    public String getUserInfoJson(final String authCode, String callbackUri) throws IOException{
        final GoogleTokenResponse response = flow.newTokenRequest(authCode).setRedirectUri(callbackUri).execute();
        final Credential credential= flow.createAndStoreCredential(response, null);
        final HttpRequestFactory requestFactory = HTTP_TRANSPORT.createRequestFactory(credential);
        final GenericUrl url = new GenericUrl(USER_INFO_URL);
        final HttpRequest request = requestFactory.buildGetRequest(url);
        request.getHeaders().setContentType("application/json");
        final String jsonIdentity = request.execute().parseAsString();
        return jsonIdentity;
    }


        /*
         * This method generates a unique session token to prevent cross-site request forgery (CSRF)
         */

        /*
        private void generateStateToken(){
                SecureRandom sr = new SecureRandom();
                stateToken = "google;"+sr.nextInt();
        }

        public String getStateToken(){
                return stateToken;
        }*/
}