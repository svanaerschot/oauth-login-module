package com.vic.alfresco;

import java.io.IOException;
import java.util.Scanner;

import com.google.gson.Gson;
import com.vic.profile.GoogleProfileInfo;
import org.apache.commons.lang.StringUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;
import org.scribe.builder.ServiceBuilder;
import org.scribe.builder.api.GoogleApi;
import org.scribe.builder.api.LinkedInApi;
import org.scribe.model.*;
import org.scribe.oauth.OAuthService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created with IntelliJ IDEA.
 * User: Vic
 * Date: 4/09/13
 * Time: 15:29
 * To change this template use File | Settings | File Templates.
 */


public class ScribeAuthHelper implements AuthHelper{

    //LinkedIn

    private static final String CLIENT_ID= "ko806ps9qcrr";
    private static final String CLIENT_SECRET= "Fqdpy2ReZ9FL0sA8";
    private static final String CALLBACK_URI = "";
    private static final String PROTECTED_RESOURCE_URL = "http://api.linkedin.com/v1/people/~:(id,e-mailaddress,first-name,last-name)";


    //Twitter
    /*
    private static final String CLIENT_ID= "GWd9hjhPTYPbshFgEBjBw";
    private static final String CLIENT_SECRET = "QhlxkpP1lcKVkxvrZbTu5M1asIi1WxDu2jShCGgIo";
    private static final String PROTECTED_RESOURCE_URL = "http://api.twitter.com/1.1/account/verify_credentials.json";
      */

    //Google
         /*
    private static final String CLIENT_ID= "129700645235-t83h25f7s5q6acti4p5kqsejfpt8144q.apps.googleusercontent.com";
    private static final String CLIENT_SECRET = "3iZ4QTNccx1ba-_Isd92meeY";
    private static final String PROTECTED_RESOURCE_URL = "https://www.googleapis.com/oauth2/v1/userinfo";
    private static final String SCOPE= "https://www.googleapis.com/auth/userinfo.profile";
           */
    private OAuthService service;
   // private Token requestToken;
    private String callbackURL;

    public ScribeAuthHelper(String callbackURL){
        this.callbackURL = callbackURL;
        //add api class
    }

    private OAuthService getOAuthService(){
        service = new ServiceBuilder()
                .provider(LinkedInApi.class)
                .apiKey(CLIENT_ID)
                .apiSecret(CLIENT_SECRET)
              //  .scope(SCOPE)
                .callback(callbackURL)
                .build();


        return service;
    }

    
    public String buildLoginUrl(String callbackUri, Token requestToken) {
        return service.getAuthorizationUrl(requestToken);
    }

    public Token getRequestToken(){
        return getOAuthService().getRequestToken();
    }



    public String getUserInfoJson(String authcode, String callbackUri) throws IOException {
        OAuthService oauthService = service;
        Verifier verifier = new Verifier(authcode);

        OAuthRequest request = new OAuthRequest(Verb.GET, PROTECTED_RESOURCE_URL);
        Token accessToken = oauthService.getAccessToken(null, verifier);

        oauthService.signRequest(accessToken, request);
        Response oauthResponse = request.send();

        return oauthResponse.getBody();
    }

   
    public String getUserInfo(String authcode, String callbackUri, Token requestToken) throws IOException {
        Verifier verifier = new Verifier(authcode);

        //Trading requestToken for AccessToken
        //!!!!Code stops working here!!!!!!
        // oauth_problem=permission_unknown
        Token accessToken = service.getAccessToken(requestToken, verifier);


        OAuthRequest request = new OAuthRequest(Verb.GET, PROTECTED_RESOURCE_URL);
        service.signRequest(accessToken, request);
        Response oauthResponse = request.send();
        if(getOAuthService().getClass() == LinkedInApi.class){
        return xmlToJson(oauthResponse.getBody());
        }
        else{
            return oauthResponse.getBody();
         }
    }


    /*
    Google returns userinfo as JSON
    LinkedIn as XML
    OAuthSSOAuthenticationFilter expects JSON
     */

    public String xmlToJson(String xml){
        try {
            JSONObject xmlJSONObj = XML.toJSONObject(xml);
            return xmlJSONObj.toString();
        } catch (JSONException je) {
            System.out.println(je.toString());
            return null;
        }
    }

}
